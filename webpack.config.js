const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
	template: "./src/index.html",
	filename: "./index.html",
	hash: true,
	title: 'My Awesome application',
	myPageHeader: 'Hello World',
});

module.exports = {
  entry : ".\\src\\index.js",
  output: {
   
    publicPath: '/'
  },
   devServer: {
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
		   options: {
          presets: ['@babel/react']
        }
        }
      },
	 {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
		    	  options: {
              modules: true,
            },
		    	  
          }
        ]
      }
    ]
  },
  
    externals: {
        // global app config object
        config: JSON.stringify({
            apiUrl: 'http://localhost:44931'
        })
    },
  plugins: [htmlPlugin]
};