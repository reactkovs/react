import { createStore } from "redux";
import userDataReducer from "./components/_reducers/reducers";

export default createStore(userDataReducer);