import React from "react";
import ReactDOM from "react-dom";
import {App} from './App';
import { Provider } from "react-redux";
import { store } from './components/_helpers';

const rootElement = document.getElementById("index");

ReactDOM.render(
      <Provider store={store}>
          <App />
       </Provider>,
       rootElement
);