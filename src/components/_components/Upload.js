import config from 'config';
import React from 'react'
import axios, { post } from 'axios';
import { authHeader } from '../_helpers';

class UploadForm extends React.Component {

  
  constructor(props) {
    super(props);
    this.state = {
      type: this.props.type,
      id: 'Upload',
      file: null,
    };

    console.log(this.props)
  }

  submit(e) {
    e.preventDefault();

    const url = `${config.apiUrl}/${this.state.id}?`;

    const formData = new FormData();
    formData.append('body', this.state.file);
    
    const config1 = {
      headers: authHeader()
    };
    console.log(config1)
    return axios.post(url, formData, config1);


  }

  setFile(e) {
    this.setState({ file: e.target.files[0] });
    console.log(e.target)
  }

  render() {
    return (
      <form onSubmit={e => this.submit(e)}>
        <h1>File Upload</h1>
        <input type="file" onChange={e => this.setFile(e)} />
        <button type="submit">Upload</button>
      </form>
    );
  }
}
 
export {UploadForm};