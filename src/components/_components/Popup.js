import React from 'react';  
import '../../styles/popup.css';  

import {UploadForm } from '../_components/Upload'


class Popup extends React.Component {  

	toggleModal () {
	  this.setState({
		isOpen: !this.state.isOpen
	  });
	}
	
	render() {  
		return (  
			<div className='popup'>  
				<div className='popup_inner'>  
				<h1>{this.props.text}</h1>  
				<button onClick={this.props.closePopup}>close me</button>  
				</div>  
			</div>  
		);  
	}  
}  

export {Popup as Modal};