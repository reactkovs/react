import { requestsConstants } from '../_constants';
import { requestService } from '../_services/request.service';
import { alertActions } from './';
import { history } from '../_helpers';

export const requestActions = {
    getRequests,
    deleteRequest
};

export const reqActions = {
    addRequest,
  
};

function getRequests() {
    return dispatch => {
        dispatch(request());

        requestService.getRequests()
            .then(
                requests => dispatch(success(requests)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: requestsConstants.GET_REQUESTS } }
    function success(requests) { return { type: requestsConstants.GET_REQUESTS_SUCCESS, requests } }
    function failure(error) { return { type: requestsConstants.GET_REQUESTS_FAILURE, error } }
}

function addRequest() { 
    return dispatch => {
        dispatch(request({}));

        requestService.addRequest()
            .then(
                request => { 
                    dispatch(success(request));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(request) { return { type: requestsConstants.REGISTER_REQUEST_REQUEST, request } }
    function success(request) { return { type: requestsConstants.REGISTER_REQUEST_SUCCESS, request } }
    function failure(error) { return { type: requestsConstants.REGISTER_REQUEST_FAILURE, error } }
}

function deleteRequest(id) { console.log('ddd');
    return dispatch => {
        dispatch(request(id));

        requestService.deleteRequest(id)
            .then(
                request => { 
                    dispatch(success(id));
                },
                error => {
                    dispatch(failure(id, error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(request) { return { type: requestsConstants.DELETE_REQUEST_REQUEST, request } }
    function success(request) {console.log('asd'); return { type: requestsConstants.DELETE_REQUEST_SUCCESS, request } }
    function failure(error) { return { type: requestsConstants.DELETE_REQUEST_FAILURE, error } }
}
