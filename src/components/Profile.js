import React from 'react';
import config from 'config';
import { connect } from 'react-redux';
import { userActions } from './_actions';	
import { Contact } from './Contact';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import styles from '../components/styles/home.css'

require('../logger');

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           profile: ''
        };
       
    }
	
	componentDidMount () {
        this.load = false;
        var vvv = this.props.getById(this.props.match.params.id);
        this.setState({vvv : vvv});
        this.load = true;
    }
    
    render() {
        const { users } = this.props;
        if (users.item){
            
            return (
                <div>
                    <div className={styles.block}>
                    <img src={`${config.apiUrl}/api/SampleData/Test?id=${users.item.image}`} alt="Logo" />
                    </div>
                    <h2>Profile</h2>
                    <Contact  name="xjuj"/>
                        <Button>xujnia</Button>
                    <div>
                        <button className={styles.button}>Vanity Button</button>
                        <button className={styles.uuu}>Vanity Button</button>
                    </div>
                </div>
            
            );
        }else{
            return (<div></div>)
        }
    }
}

function mapState(state) {
    const { users } = state;
    return { users };
}

const actionCreators = {
    getById: userActions.getById,
    deleteUser: userActions.delete
}

const profilePage = connect(mapState, actionCreators)(Profile);
export { profilePage as Profile };