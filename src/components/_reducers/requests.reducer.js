import { requestsConstants } from '../_constants';
import { store } from '../../components/_helpers';

export function requests(state = {}, action) {
  //console.log(store.getState());
  if(store){  console.log('dddd');

  console.log(store.getState.authorization);
  console.log(store.getState);
  console.log(store);
  }
  switch (action.type) {
    
    case requestsConstants.GET_REQUESTS:
      return Object.assign({}, state, {
        requesting : action.requests
      });
     
    case requestsConstants.GET_REQUESTS_SUCCESS:
      return Object.assign({}, state, {
        items : action.requests
      });
    case requestsConstants.GET_REQUESTS_FAILURE:
      return {
        error: action.error
      };
      case requestsConstants.REGISTER_REQUEST_REQUEST:
        return Object.assign({}, state, {
          request: action.item
        });
      case requestsConstants.REGISTER_REQUEST_SUCCESS:
        var st =  Object.assign({}, state);
        st.items.push(action.request);
        return st;
    case requestsConstants.REGISTER_REQUEST_FAILURE:
      return {
        error: action.error
      };
    case requestsConstants.DELETE_REQUEST_REQUEST:
      return {
        ...state,
        items: state.items.map(req =>
          req.id === action.id
            ? { ...req, deleting: true }
            : req
        )
      };
    case requestsConstants.DELETE_REQUEST_SUCCESS:

      return {
        ...state,
        items: state.items.filter(user => user.id !== action.request)
      };
    case requestsConstants.DELETE_REQUEST_FAILURE:
      return {};
    case requestsConstants.ACCEPT_REQUEST:
      return {};
    case requestsConstants.ACCEPT_SUCCESS:
      return {};
    default:
      return state
  }
}