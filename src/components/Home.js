
import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import { UploadForm } from './_components/Upload'
import config from 'config';
import {Modal} from './_components/Popup';

import { userActions } from './_actions';

/*
const mapDispatchToProps = dispatch => ({ 
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
   dispatch(showModal({ modalProps, modalType }))
  }
 })
*/


class Home extends React.Component {
	
	constructor(props) {
        super(props);
        
        const modalIsOpen = false;
        const setIsOpen = false;
     
        this.state = {
          showModal: false
        };
        this.buttonClick = this.buttonClick.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this); 
    }

	
    componentDidMount() {
		    this.props.getUsers();
    }

    handleCloseModal(){
      this.setState({ showModal: false });

    }
  	buttonClick(event) {
       this.setState({ showModal: true });

/*
      this.props.showModal({
       open: true,
       title: 'Alert Modal',
       message: MESSAGE,
       closeModal: this.closeModal
     }, 'alert')
  */
    }
  
	openModal() {
		setIsOpen(true);
	}
	
    closeModal(){
      
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }

    render() {
        const { user, users } = this.props;
        const divStyle = {
          borderRadius: '30px' 
        };
        return (
            <div className="col-md-6 col-md-offset-3">

                <h1>Hi <Link to={`/profile/${user.id}`}>{user.firstName}!</Link></h1>
                <p>You're logged in with React!!</p>
                <span> <Link to={"/requests"}>Requests</Link></span >
                <span> <Link to={"/users"}> Users</Link></span>

                <h3>All registered users:</h3>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
			        	{users.items &&
                    <ul>
                        {users.items.map((item, index) =>
                         
                            <li key={index.toString()}>
                              <div>
                                <div>
                                  <img src={`${config.apiUrl}/api/SampleData/Test?id=${item.image?item.image:'00000000-0000-0000-0000-000000000000'}`} alt="Logo" />
                                </div>
                                <div>
                                  {item.image}
								                    <Link to={`/profile/${item.id}`}>{item.name} {item.id}</Link>
                               </div>
                               </div>
                            </li>
                        )}
                    </ul>
                }
                <p>

          <ReactModal 
            isOpen={this.state.showModal}
            contentLabel="Minimal Modal Example"
          >
          upload file example
          <UploadForm type="profile"></UploadForm>
          <button onClick={this.handleCloseModal}>Close Modal</button>
        </ReactModal>
				<button onClick={this.buttonClick}>
				  Activate Modal
				</button>

                  <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(Home);
export { connectedHomePage as Home };

