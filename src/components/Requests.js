import React from 'react';
import { connect } from 'react-redux';
import { requestActions } from './_actions';
import { reqActions } from './_actions';
import config from 'config';
import styles from '../components/styles/requests.css'

class Requests extends React.Component {
	
	  constructor(props) {
        super(props);
        
        this.addRequest = this.addRequest.bind(this);
        this.deleteRequest = this.deleteRequest.bind(this);
    }
	
    componentDidMount() {
        this.props.getRequests();
        var users = this.props.users;
        var user = this.props.user
      

        if(!users){
          console.log('here')
          this.props.getUsers();
        }
    }

    addRequest(){
      this.props.addRequest();
    }

    deleteRequest(){
      const {requests, user} = this.props
      requests.items.forEach(element => {
        if (element.userId === user.id ){
          console.log(element)
          this.props.deleteRequest(element.id);
        }
      });
     
    }

    render() {
      const { requests, user, users } = this.props;
 
       console.log(requests);
        return (
          
           <div>Request 

            <h3>All Games Requests:</h3><a className="btn btn-primary" onClick={this.addRequest} href="#">Add Request</a>
            <a className="btn btn-primary" onClick={this.deleteRequest} href="#">Remove Request</a>
                {requests.requesting && <em>Requesting games...</em>}
                {requests.error && <span className="text-danger">ERROR: {requests.error}</span>}
			        	{requests.items &&
                    <div>
                        {requests.items.map((item, index) =>
                         
                         <div className={styles.reqBlock} key={index.toString()}>
                           <div>
                              <div className="fl">
                               <img className={styles.imageW} src={`${config.apiUrl}/api/SampleData/Test?id=${item.imageId}`} alt="Logo" />
                              </div>
                              <div className="fr">
                                  <h4>Request Id {item.id}</h4>
                                  <h4>>User id {item.userId}</h4>
                                  <h4>User name {users.name}</h4>
                                  <a href="#" className="btn btn-primary">Accept Request</a>
                              </div>
                              <div className="cb"></div>
                                <div>
                                  {index.toString()}                            
                              </div>
                            </div>
                          </div>
                        )}
                    </div>
                }
           </div>
        );
    }
}

function mapState(state) {
    const { requests, request, users, authentication } = state;
    const { user } = authentication;
    console.log(user)
    console.log('asd')
    return { requests, request, users, user };
}

const actionCreators = {
    getRequests: requestActions.getRequests,
    deleteRequest: requestActions.deleteRequest,
    addRequest: reqActions.addRequest
}

const connectedRequestsPage = connect(mapState, actionCreators)(Requests);
export { connectedRequestsPage as Requests };

