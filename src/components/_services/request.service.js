import config from 'config';
import { authHeader } from '../_helpers';

export const requestService = {
    getRequests,
    addRequest,
    deleteRequest,
    logout
};

function getRequests() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/api/Request/ListGameRequests`, requestOptions)
        .then(handleResponse)
        .then(requests => {
            return requests;
        });
}

function addRequest() {
    const requestOptions = {
        method: 'POST',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/api/request/AddGameRequest`, requestOptions).then(handleResponse);
}

function deleteRequest(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
        body: {requestId: id}
    }

    return fetch(`${config.apiUrl}/api/Request`, requestOptions).then(handleResponse);
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}