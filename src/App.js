import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.css';

import { history } from './components/_helpers';
import { alertActions } from './components/_actions';
import { PrivateRoute } from './components/_components'
import {Home} from './components/Home';
import {Login} from './components/Login';
import {Profile} from './components/Profile';
import {Register} from './components/Register';
import {Contact} from './components/Contact';
import {Requests} from './components/Requests';


class App extends Component {
	constructor(props) {
		super(props);

		history.listen((location, action) => {
			// clear alert on location change
			this.props.clearAlerts();
		});
	}	
	
	render() {
	
		return (
		  <div className="jumbotron">
			  <Header/>
			<div className="container">
				<div className="col-sm-8 col-sm-offset-2">
						{alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
                        }
					<Router history={history}>
						<div>
							<Switch>
							    <PrivateRoute exact path="/" component={Home} />
								<Route path="/login" component={Login} />
								<Route path="/register" component={Register} />
								<PrivateRoute path="/profile/:id" component={Profile} />
								<PrivateRoute path="/contact" component={Contact} />
								<PrivateRoute path="/requests" component={Requests} />
								<Redirect from="*" to="/" />
							</Switch>
						</div>
					 </Router>
				</div>

			</div>
			<Footer/>				
		</div>
			
		);
	}
}	

const Header = () => {
	return (
		<div>
			<a href='#s'> eto </a>
			<a href='#s'> prosto </a>
			<a href='#s'> kajuk </a>
		</div>
	)

}

const Footer = () => {
	return (
		<div>
			<p><a href='#s'> eto </a></p>
			<p><a href='#s'> prosto </a></p>
			<p><a href='#s'> kajuk </a></p>
		</div>
	)

}


function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };
